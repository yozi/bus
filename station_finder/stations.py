from sklearn.cluster import MeanShift, estimate_bandwidth
import numpy as np
import db
import json

__author__ = 'Anton Trusov <inhavk@gmail.com>'

STATION_RADIUS = 150
EQUALS_RADIUS = 3
BANDWIDTH_QUANTILE = 0.8
MINIMAL_WEIGHT = 5

conn = db.conn

class Stations(object):
    @classmethod
    def solve_station_finder(cls, route, direction):
        cursor = db.get_cursor(conn)

        # возмем точки, ассоциированные с остановкой
        cursor.execute(
            '''SELECT track.id FROM track
                WHERE route=%(route)s AND avg_speed=0 AND from_start = %(dir)s''',
            {'route': route, 'dir': direction})
        tracks_ids = cursor.fetchall()
        tracks_ids = [track_id[0] for track_id in tracks_ids]
        print(len(tracks_ids))
        phase_first = cls.clustering_station_tracks(tracks_ids)
        print("First phase complete!", len(phase_first))
        return cls.mean_clustering(phase_first)

    @staticmethod
    def clustering_station_tracks(station_tracks_ids, time_interval=None):
        cursor = db.get_cursor(conn)

        clustered_points = []  # точки, которые уже в кластерах
        centroids = []  # центры кластеров

        point_size = len(station_tracks_ids) / 40
        while len(clustered_points) != len(station_tracks_ids):
            print("\r [{:<40}]".format("".join(["#" for i in range(int(len(clustered_points) // point_size))])), end="")
            # выберем случайную точку, как центр кластера
            cursor.execute('''SELECT location FROM track WHERE id = ANY(%s) AND NOT id = ANY(%s) LIMIT 1''',
                           (station_tracks_ids, clustered_points))
            centroid = cursor.fetchone()
            if centroid is None:
                break
            centroid = centroid[0]

            old_centroid = None
            while True:
                if old_centroid is not None:
                    cursor.execute('SELECT ST_DWithin(%(first)s, %(second)s, %(radius)s)',
                        {'first': old_centroid, 'second': centroid, 'radius': EQUALS_RADIUS})
                    complete = int(cursor.fetchone()[0])
                    if complete:
                        break

                old_centroid = centroid
                # вычислим новый центр кластера
                cursor.execute('''SELECT track.route, ST_Centroid(ST_Multi(ST_Union(track.location::geometry))) as center FROM track
                                  WHERE ST_DWithin(track.location, %(point)s, %(r)s)
                                  AND id = ANY(%(ids_full)s) AND NOT id = ANY(%(ids_clustered)s)
                                  GROUP BY track.route''',
                               {'point': centroid,
                                'r': STATION_RADIUS,
                                'ids_full': station_tracks_ids,
                                'ids_clustered': clustered_points})
                centroid = cursor.fetchone()[1]

            # возьмем точки, ассоциированные с кластером
            cursor.execute('''SELECT track.id, ST_Y(track.location::GEOMETRY), ST_X(track.location::GEOMETRY) FROM track
                              WHERE ST_DWithin(track.location, %(point)s, %(r)s)
                              AND id = ANY(%(ids_full)s) AND NOT id = ANY(%(ids_clustered)s)''',
                            {'point': centroid,
                             'r': STATION_RADIUS,
                             'ids_full': station_tracks_ids,
                             'ids_clustered': clustered_points})

            points = []
            cluster_points = []
            for track_id in cursor.fetchall():
                cluster_points.append(track_id[0])
                points.append((track_id[1], track_id[2]))

            clustered_points = list(set(clustered_points).union(set(cluster_points)))
            centroids.append({'centroid': centroid, 'points': points})
        print()
        return centroids

    @staticmethod
    def mean_clustering(clusters):
        out = []
        success = 0
        failed = 0
        len_clusters = len(clusters)
        len_complete = 0
        point_size = len_clusters / 40
        for raw_cluster in clusters:
            print("\r [{:<40}] {}".format("".join(["#" for i in range(int(len_complete // point_size))]),
                                          len(raw_cluster["points"])), end="")
            if len(raw_cluster["points"]) < MINIMAL_WEIGHT:
                continue
            try:
                points = np.array(raw_cluster["points"])
                bandwidth = estimate_bandwidth(points, quantile=BANDWIDTH_QUANTILE)
                ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
                ms.fit(points)

                cluster_centers = ms.cluster_centers_
                n_clusters_ = len(np.unique(ms.labels_))

                for i, k in zip(range(n_clusters_), cluster_centers):
                    out.append((k[0], k[1]))

                success += 1
            except ValueError:
                failed += 1
            len_complete += 1
        print()
        print("Second phase complete!", len(out))
        print(success, failed)
        return out


if __name__ == '__main__':
#    from analyser.stations import mark_point_direction
#    Directions.mark_point_direction('36', ((55.556708, 38.620766), (55.565171, 38.227452)))
    result = Stations.solve_station_finder("36", "True")

    out = {"type": "FeatureCollection", "features": []}
    for i, k in zip(range(len(result)), result):
        geometry = {"type": "Point", "coordinates": [k[0], k[1]]}
        properties = {"clusterCaption": "True", "hintContent": "True"}
        record = {"type": "Feature", "id": i, "geometry": geometry, "properties": properties}
        out["features"].append(record)

    out = json.dumps(out, sort_keys=True)
    print(out)