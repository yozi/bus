import argparse
import db
import json
import os
import sys
from config import DATABASE
from parser import TrackParser, StationParser, TrackSeparator
from station_finder.stations import Stations
from analyser.stations import mark_point_direction


def handle_db(args_: argparse.Namespace):
    if args_.command == 'init':
        db.init_tables()
    elif args_.command == 'info':
        conn = db.get_conn()
        cursor = db.get_cursor(conn)
        cursor.execute('SELECT pg_size_pretty( pg_database_size( %s ) )', (DATABASE['db_name'], ))
        db_size = cursor.fetchone()
        print('db_size {}'.format(db_size[0]))


def handle_parser(args_: argparse.Namespace):
    if args_.parser == 'track':
        TrackParser.parse_file(path=args_.file)
    elif args_.parser == 'station':
        StationParser.parse_file(path=args_.file)
    elif args_.parser == 'separator':
        TrackSeparator.sep_file(path=args_.file)

def handle_split(args_: argparse.Namespace):
    mark_point_direction(args_.route, ((args_.lat1, args_.long1), (args_.lat2, args_.long2)))

def handle_stations(args_: argparse.Namespace):
    result = []
    for direction in ["True", "False"]:
        result += Stations.solve_station_finder(args_.route, direction)

    result = json.dumps(result, sort_keys=True)

    with open(args_.file, "w") as f:
        f.write(result)

if __name__ == "__main__":
    sys.path.append(os.path.abspath(os.path.dirname(__file__)))
    parser = argparse.ArgumentParser(prog='BUS STATION PROGRAM')

    parser.add_argument('--foo', action='store_true', help='foo help')
    subparsers = parser.add_subparsers(help='sub-command help')

    parser_db = subparsers.add_parser('db', help='Some commands for db such `init` or `info`')
    parser_db.add_argument('command', type=str, help='Init DB', default=False, choices=('init', 'info'))
    parser_db.set_defaults(handler=handle_db)

    parser_parser = subparsers.add_parser('parse', help='Handle files: parse data, separate files or something else')
    parser_parser.add_argument('parser', type=str, help='Kind of parser: `track`, `station` or `separator`',
                               choices=('track', 'station', 'separator'))
    parser_parser.add_argument('file', type=str, help='Parse file')

    parser_split = subparsers.add_parser('split', help='')
    parser_split.add_argument('route', type=str, help='Route number', default=False)
    parser_split.add_argument('lat1', type=float, help='Start lat', default=False)
    parser_split.add_argument('long1', type=float, help='Start long', default=False)
    parser_split.add_argument('lat2', type=float, help='Finish lat', default=False)
    parser_split.add_argument('long2', type=float, help='Finish long', default=False)
    parser_split.set_defaults(handler=handle_split)

    parser_stations = subparsers.add_parser('stations', help='')
    parser_stations.add_argument('route', type=str, help='Route number', default=False)
    parser_stations.add_argument('file', type=str, help='Output file')
    parser_stations.set_defaults(handler=handle_stations)

    parser_parser.set_defaults(handler=handle_parser)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()  # INFO: because you should provider one of subparsers argument
    else:
        args = parser.parse_args()
        args.handler(args)
