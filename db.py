import psycopg2
from config import DATABASE


def get_conn() -> psycopg2.extensions.connection:
    return psycopg2.connect(database=DATABASE['db_name'], user=DATABASE['user'],
                            password=DATABASE['pass'], host=DATABASE['host'])

conn = get_conn()


def get_cursor(conn_) -> psycopg2.extensions.cursor:
    return conn_.cursor()


def init_tables():
    print('just load /data/db_init.backup with pgAdmin 3')
