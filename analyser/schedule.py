"""
Анализирум треки и получаем расписание для станций
"""
from collections import defaultdict
from datetime import datetime
import db

SCHEDULE_STATION_RADIUS = 200  # радиус "покрытия" станции в метрах
SCHEDULE_TIME_INTERVAL = 60 * 30  # интервал чаще которого один автобус не мог посещать одну станции
FROM_START_KEY = True
TO_START_KEY = False

conn = db.conn


def get_schedule(route):
    cursor = db.get_cursor(conn)
    cursor.execute('SELECT  id, ST_Y(location::GEOMETRY), ST_X(location::GEOMETRY) FROM station WHERE mark=%(mark)s',
                   {'mark': '{route}route.tsv'.format(route=route)})
    stations = cursor.fetchall()
    stations_dict = {}

    bus_visits = {}
    for station in stations:
        stations_dict[station[0]] = station
        bus_visits[station[0]] = bus_station_visits(station, route)
        pass
    pass
    print('bus_visits complete ')
    bus_visits_timeline = {}
    for station_id in bus_visits:
        for direction in bus_visits[station_id]:
            if direction not in bus_visits_timeline:
                bus_visits_timeline[direction] = dict()
            for bus_uuid in bus_visits[station_id][direction]:
                if bus_uuid not in bus_visits_timeline[direction]:
                    bus_visits_timeline[direction][bus_uuid] = list()
                for visit_cluster in bus_visits[station_id][direction][bus_uuid]:
                    bus_visits_timeline[direction][bus_uuid].append((station_id, visit_cluster))
                bus_visits_timeline[direction][bus_uuid].sort(key=lambda x: x[1]['min_t'])

    # исправляем ошибки связанные со стоянкой на остановке дольше чем SCHEDULE_TIME_INTERVAL
    print('bus_visits_timeline complete ')
    bus_fixed_visits = {}
    for direction in bus_visits_timeline:
        for bus_uuid in bus_visits_timeline[direction]:
            prev_station_id = None
            bus_prev_time = {}
            for station_id, visit_cluster in bus_visits_timeline[direction][bus_uuid]:
                if prev_station_id == station_id:
                    bus_fixed_visits[station_id][direction][bus_uuid][-1]['max_t'] = visit_cluster['max_t']
                    bus_fixed_visits[station_id][direction][bus_uuid][-1]['count'] += visit_cluster['count']

                else:
                    if station_id in bus_prev_time \
                            and (visit_cluster['max_t'] - bus_prev_time[station_id]).total_seconds() < SCHEDULE_TIME_INTERVAL:
                        bus_fixed_visits[station_id][direction][bus_uuid][-1]['max_t'] = visit_cluster['max_t']
                        bus_fixed_visits[station_id][direction][bus_uuid][-1]['count'] += visit_cluster['count']
                    else:
                        if station_id not in bus_fixed_visits:
                            bus_fixed_visits[station_id] = dict()
                        if direction not in bus_fixed_visits[station_id]:
                            bus_fixed_visits[station_id][direction] = dict()
                        if bus_uuid not in bus_fixed_visits[station_id][direction]:
                            bus_fixed_visits[station_id][direction][bus_uuid] = list()
                        bus_fixed_visits[station_id][direction][bus_uuid].append(visit_cluster)
                prev_station_id = station_id
                bus_prev_time[station_id] = visit_cluster['max_t']
                pass
    print('bus_fixed_visits complete ')
    schedule = {}
    for station_id in bus_fixed_visits:
        if station_id not in schedule:
            schedule[station_id] = {FROM_START_KEY: dict(), TO_START_KEY: dict(), 'info': stations_dict[station_id]}
        for direction in bus_fixed_visits[station_id]:
            for bus_uuid in bus_fixed_visits[station_id][direction]:
                for visit_cluster in bus_fixed_visits[station_id][direction][bus_uuid]:
                    day = visit_cluster['max_t'].day
                    if day not in schedule[station_id][direction]:
                        schedule[station_id][direction][day] = list()
                    schedule[station_id][direction][day].append(visit_cluster['max_t'].time())
            for day in schedule[station_id][direction]:
                schedule[station_id][direction][day].sort()
    print('schedule complete ')
    return schedule


def bus_station_visits(station, route):
    cursor = db.get_cursor(conn)
    station_id, station_lat, station_lon = station
    cursor.execute('SELECT DISTINCT uuid FROM track WHERE route=%(route)s AND from_start IS NOT NULL', {'route': route})
    bus_uuids = [row[0] for row in cursor]
    bus_visits = {
        FROM_START_KEY: dict(),
        TO_START_KEY: dict()
    }

    for bus_uuid in bus_uuids:
        for from_start in (False, True):
            # возмем точки, ассоциированные с остановкой
            direction = FROM_START_KEY if from_start else TO_START_KEY
            cursor.execute(
                '''SELECT track.id FROM track
                    WHERE ST_DWithin(track.location,
                      ST_GeographyFromText('SRID=4326;POINT(%(lon)s %(lat)s)'),  %(r)s
                    ) AND route=%(route)s AND from_start=%(from_start)s AND uuid=%(uuid)s''',
                {'r': SCHEDULE_STATION_RADIUS, 'lat': station_lat, 'lon': station_lon, 'route': route,
                 'from_start': from_start, 'uuid': bus_uuid})
            points = [row[0] for row in cursor]
            clusters = clustering_points(points, SCHEDULE_TIME_INTERVAL)
            bus_visits[direction][bus_uuid] = clusters

    return bus_visits


def clustering_points(points, time_interval):
    """
    Разбить точки на отрезки на оси времени
    :param points:
    :param time_interval: размер отрезков, на котоыре происходит разбиение
    :return: словарь с полями 'min_t', 'max_t', 'count' - начало, конец и количество точек отрезка
    """
    cursor = db.get_cursor(conn)
    clustered_points = []  # точки, которые уже в кластерах
    clusters = []  # кластеры, в данном случае кластер - это время проведенное на остановке

    while len(clustered_points) != len(points):
        # выберем случайную точку, как центр кластера
        cursor.execute('''SELECT time FROM track WHERE id = ANY(%s) AND NOT id = ANY(%s) LIMIT 1''',
                       (points, clustered_points))
        centroid_time = cursor.fetchone()[0]
        cluster_points = points
        old_centroid_time = None
        while centroid_time != old_centroid_time:
            old_centroid_time = centroid_time
            # возьмем точки, ассоциированные с кластером
            cursor.execute('''SELECT id FROM track
                              WHERE id = ANY(%(points_ids)s) AND NOT id=ANY(%(clustered_points)s)
                              AND time BETWEEN %(time_l)s AND %(time_r)s''',
                           {'points_ids': cluster_points,
                            'time_l': datetime.fromtimestamp(centroid_time.timestamp() - time_interval),
                            'time_r': datetime.fromtimestamp(centroid_time.timestamp() + time_interval),
                            'clustered_points': clustered_points,
                           })
            cluster_points = cursor.fetchall()
            cluster_points = [row[0] for row in cluster_points]

            # вычислим новый центр кластера
            cursor.execute('''SELECT to_timestamp(AVG(extract(EPOCH FROM  time))) AT TIME ZONE 'utc' FROM track
                              WHERE id = ANY (%s)''',
                           (cluster_points, ))
            centroid_time = cursor.fetchone()[0]
            pass
        else:
            clustered_points = list(set(clustered_points).union(set(cluster_points)))

            cursor.execute('SELECT MIN(time), MAX(time) FROM track WHERE id = ANY(%s)', (cluster_points,))
            min_t, max_t = cursor.fetchone()
            clusters.append({
                'min_t': min_t,
                'max_t': max_t,
                'count': len(cluster_points),
            })
        pass

    return sorted(clusters, key=lambda v: v['min_t'])


if __name__ == '__main__':
    schedule_ = get_schedule('549')
    pass