import db

STATION_RADIUS = 100
conn = db.conn

from datetime import datetime
import db


SCHEDULE_STATION_RADIUS = 300
SCHEDULE_TIME_INTERVAL = 60 * 20


def clustering_points(points, time_interval):
    cursor = db.get_cursor(conn)

    clustered_points = []  # точки, которые уже в кластерах
    centroids = []  # центры кластеров, в данном случае время остановок

    while len(clustered_points) != len(points):
        # выберем случайную точку, как центр кластера
        cursor.execute('''SELECT time FROM track WHERE id = ANY(%s) AND NOT id = ANY(%s) LIMIT 1''',
                       (points, clustered_points))
        centroid_time = cursor.fetchone()[0]
        cluster_points = points
        old_centroid_time = None
        while centroid_time != old_centroid_time:
            old_centroid_time = centroid_time
            # возьмем точки, ассоциированные с кластером
            cursor.execute('''SELECT id FROM track
                              WHERE id = ANY(%(tracks_ids)s) AND NOT id=ANY(%(clustered_points)s)
                              AND time BETWEEN %(time_l)s AND %(time_r)s''',
                           {'tracks_ids': cluster_points,
                            'time_l': datetime.fromtimestamp(centroid_time.timestamp() - time_interval),
                            'time_r': datetime.fromtimestamp(centroid_time.timestamp() + time_interval),
                            'clustered_points': clustered_points,
                           })
            cluster_points = cursor.fetchall()
            cluster_points = [track_id[0] for track_id in cluster_points]

            # вычислим новый центр кластера
            cursor.execute('''SELECT to_timestamp(AVG(extract(EPOCH FROM  time))) AT TIME ZONE 'utc' FROM track
                              WHERE id = ANY (%s)''',
                           (cluster_points, ))
            centroid_time = cursor.fetchone()[0]
            pass
        else:
            clustered_points = list(set(clustered_points).union(set(cluster_points)))
            centroids.append({'centroid_time': centroid_time, 'points_count': len(cluster_points)})
        pass

    return centroids



def mark_point_direction(route, terminal_stations):
    """
    Помечает точки трека как идущие "туда" или "обратно"
    Для этого нужны координаты конечных станций и маршрут рейса
    :return:
    """
    cursor = db.get_cursor(conn)
    start_station, finish_station = terminal_stations

    cursor.execute('''SELECT uuid FROM track
                      WHERE route=%(route)s AND
                      ST_DWithin(
                        location, ST_GeographyFromText('SRID=4326;POINT(%(lon)s %(lat)s)'), %(r)s
                      ) GROUP BY uuid
                    ''', {'route': route, 'r': 200, 'lat': start_station[0], 'lon': start_station[1]})
    uuids = [row[0] for row in cursor]
    for uuid in uuids:
        cursor.execute(
            '''SELECT track.id FROM track
                WHERE ST_DWithin(track.location,
                  ST_GeographyFromText('SRID=4326;POINT(%(lon)s %(lat)s)'),  %(r)s
                ) AND route=%(route)s AND uuid=%(uuid)s''',
            {'r': 200, 'lat': start_station[0], 'lon': start_station[1], 'route': route, 'uuid': uuid})
        start_points_ids = [track_id[0] for track_id in cursor]

        cursor.execute(
            '''SELECT track.id FROM track
                WHERE ST_DWithin(track.location,
                  ST_GeographyFromText('SRID=4326;POINT(%(lon)s %(lat)s)'),  %(r)s
                ) AND route=%(route)s AND uuid=%(uuid)s''',
            {'r': 200, 'lat': finish_station[0], 'lon': finish_station[1], 'route': route, 'uuid': uuid})
        finish_points_ids = [track_id[0] for track_id in cursor]

        start_schedule = clustering_points(start_points_ids, SCHEDULE_TIME_INTERVAL)
        finish_schedule = clustering_points(finish_points_ids,SCHEDULE_TIME_INTERVAL)
        start_schedule.sort(key=lambda i: i['centroid_time'])
        finish_schedule.sort(key=lambda i: i['centroid_time'])

        times = [(time['centroid_time'], True) for time in start_schedule]
        times += [(time['centroid_time'], False) for time in finish_schedule]
        times.sort(key=lambda t_: t_[0])

        """
            Маркируем только перемещения от одной конечки до другой, в противном случае это не был  рейс
            Также не маркируем перемещения во время обеденного перерыва (когда дважды подряд посещена та же конечка)
        """
        while len(times) >= 2:
            time, from_start = times.pop(0)
            second_time, second_direction = times[0]
            if from_start != second_direction:
                cursor.execute('''UPDATE track SET from_start = %(from_start)s
                                  WHERE route=%(route)s AND uuid=%(uuid)s AND time BETWEEN %(start_time)s AND %(end_time)s
                                ''',
                               {
                                   'from_start': from_start,
                                   'route': route,
                                   'uuid': uuid,
                                   'end_time': second_time,
                                   'start_time': time,
                               })
                conn.commit()
            else:
                continue
    pass


if __name__ == '__main__':
    # остановки: 49км - Автовокзал раменское (49км не обозначен в яндекс расписаниях)
    # https://rasp.yandex.ru/thread/36B_f9736892t9736654_mta?tt=bus
    mark_point_direction('36', ((55.556708, 38.620766), (55.565171, 38.227452)))

