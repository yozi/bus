from datetime import datetime, time
import json
import sys
import os
import psycopg2
from analyser.schedule import get_schedule
from config import DATABASE

sys.path.append(os.path.dirname(__file__))
from flask import Flask, render_template, g

app = Flask(__name__)
app.config.from_object('config')


@app.teardown_appcontext
def close_connection(exception):
    conn_ = getattr(g, '_database', None)
    if conn_ is not None:
        conn_.close()


def get_conn() -> psycopg2.extensions.connection:
    conn_ = getattr(g, '_database', None)
    if conn_ is None:
        conn_ = g._database = psycopg2.connect(database=DATABASE['db_name'], user=DATABASE['user'],
                                               password=DATABASE['pass'], host=DATABASE['host'])
    return conn_


def get_cursor(conn_) -> psycopg2.extensions.cursor:
    return conn_.cursor()


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/heat')
def heat_map():
    conn = get_conn()
    cursor = get_cursor(conn)
    cursor.execute('''SELECT id, ST_Y(location::GEOMETRY), ST_X(location::GEOMETRY)
                      FROM STATION WHERE mark='549route.tsv' ORDER BY id;''')
    stations = ((1, 55.556708, 38.620766), (2, 55.565171, 38.227452))
    stations = cursor.fetchall()

    points = []

    cursor.execute(
        '''SELECT ST_Y(location::GEOMETRY), ST_X(location::GEOMETRY) FROM track
           WHERE route='549' ''',
        {
            'r': int(100),  # 100 метров,
            'time1': datetime(2015, 4, 12, 9, 19, 41),
            'time2': datetime(2015, 4, 12, 10, 35, 41),
        }
    )
    station_points = cursor.fetchall()
    points += station_points

    return render_template('heat.html', points=json.dumps(points), stations=json.dumps(stations))


@app.route('/time')
def time_map():
    conn = get_conn()
    cursor = get_cursor(conn)
    cursor.execute('''SELECT id, ST_Y(location::GEOMETRY), ST_X(location::GEOMETRY)
                      FROM STATION
                      WHERE mark=%(mark)s
                      ORDER BY id;
                      ''', {'mark': '36route.tsv'})
    stations = cursor.fetchall()

    points = []
    for id_, latitude, longitude in stations:
        if id_ == 491261:
            print('station #{id_}'.format(id_=id_))
            print(latitude, longitude)

        cursor.execute(
            '''SELECT ST_Y(location::geometry), ST_X(location::geometry) FROM track
               WHERE ST_DWithin(ST_GeographyFromText('SRID=4326;POINT(%(longitude)s %(latitude)s)'), location, %(r)s)
               AND route=%(route)s AND uuid=%(uuid)s AND from_start=%(from_start)s
               AND DATE_PART('day', time)=%(day)s AND DATE_PART('hour', time) BETWEEN 14 AND 15

               ''',
            {
                'longitude': float(longitude),
                'latitude': float(latitude),
                'r': int(300),  # 100 метров
                'route': '36',
                'uuid': '4980227_5566',
                'day': 22,
                'hour': 14,
                'from_start': False
            }
        )
        station_points = cursor.fetchall()
        points += station_points

    return render_template('time.html', points=json.dumps(points), stations=json.dumps(stations))


@app.route('/schedule/<string:route>')
def action_schedule(route):
    schedule = get_schedule(route)
    rebuild_schedule = {}
    for station_id in schedule:
        for direction in (False, True):
            for day in schedule[station_id][direction]:
                if day not in rebuild_schedule:
                    rebuild_schedule[day] = dict()
                if station_id not in rebuild_schedule[day]:
                    rebuild_schedule[day][station_id] = {'info': schedule[station_id]['info']}
                visites = schedule[station_id][direction][day]
                fixed_visites = []
                for visit in visites:
                    fixed_visites.append(time(hour=visit.hour+3, minute=visit.minute, second=visit.second))
                rebuild_schedule[day][station_id][direction] = fixed_visites

    return render_template('schedule.html', route=route, schedule=rebuild_schedule)


if __name__ == '__main__':
    app.run(debug=True)
